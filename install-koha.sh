#!/bin/bash
set -e

if test -f /KohaDeployed; then
	exit
fi

# Configuracion: Se activa el reinicio automatico de needrestart
sudo sed -i "s/\$nrconf{restart} = 'i';/\$nrconf{restart} = 'a';/g" /etc/needrestart/needrestart.conf

# Actualizacion de Linux.
sudo apt-get update && sudo apt-get upgrade -y
echo "Actualizacion y upgrade lista"

# Instalacion de Wget y gnupg2.
sudo apt-get install wget gnupg2 -y
echo "Wget y gnuog2 instalados"

# Instalacion de llave y repositorio Koha
sudo wget -qO - https://debian.koha-community.org/koha/gpg.asc | sudo gpg --dearmor -o /usr/share/keyrings/koha-keyring.gpg
echo 'deb [signed-by=/usr/share/keyrings/koha-keyring.gpg] http://debian.koha-community.org/koha stable main' | sudo tee /etc/apt/sources.list.d/koha.list
echo "Repositorio Koha agregado"

# Actualizacion de repositorios e Instalacion MariaDB, Apache2 y Koha.
sudo apt-get update 
sudo apt-get install mariadb-server apache2 certbot python3-certbot-apache koha-common -y
echo "Mariadb, Apache2, Certbot y Koha instalados"

# Configuraciones de koha-sites.conf
sudo sed -i 's/DOMAIN=".*/DOMAIN=".fi.uba.ar"/' /etc/koha/koha-sites.conf
sudo sed -i 's/INTRAPORT=".*/INTRAPORT="8080"/' /etc/koha/koha-sites.conf
sudo sed -i 's/INTRASUFFIX=".*/INTRASUFFIX=""/' /etc/koha/koha-sites.conf
sudo sed -i 's/OPACPORT=".*/OPACPORT="80"/' /etc/koha/koha-sites.conf
sudo sed -i 's/ZEBRA_LANGUAGE=".*/ZEBRA_LANGUAGE="es"/' /etc/koha/koha-sites.conf
sudo sed -i '/Listen 80/a Listen 8080' /etc/apache2/ports.conf
echo "Archivos de configuracion Koha y Apache2 Modificados"

# Configuraciones de Apache2
echo 'ServerName 127.0.0.1' | sudo tee -a /etc/apache2/apache2.conf
sudo a2enmod rewrite
sudo a2enmod cgi
echo "Modulos de Apache2 Activados"

# Creacion de instancia Koha
sudo koha-create --create-db catalogo
echo "Instancia catalogo creada."

# Habilitacion de instancia en apache2
sudo a2dissite 000-default
sudo a2enmod deflate
sudo service apache2 restart
sudo a2ensite catalogo
sudo service apache2 restart
echo "Sitios de catalogo activado"

# Paquete para Koha en Español
koha-translate --install es-ES

# Generacion de password
sudo koha-passwd catalogo > userkoha.txt
echo "Fichero de password maestro creado"

# sudo certbot --apache

# Configuracion: Se activa nuevamente la interfaz interectiva de needrestart
sudo sed -i "s/\$nrconf{restart} = 'a';/\$nrconf{restart} = 'i';/g" /etc/needrestart/needrestart.conf

# Evitar que el script se vuelva a ejecutar
echo "generando fichero /KohaDeployed..."
touch /KohaDeployed
echo "listo!"
